using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InputManager : MonoBehaviour
{

    static InputManager _instance;
    public static InputManager Instance { get { return _instance ?? (_instance = new GameObject().AddComponent<InputManager>()); } }

    protected bool isInGame = false;
    protected TouchControls touchControls;

    void Awake()
    {
        if (!_instance) { _instance = this; }
        Initialize();
    }

    public void Initialize()
    {
        name = "InputManager";
        DontDestroyOnLoad(this);
        touchControls = new TouchControls();
    }

    public void OnEnable() {
        touchControls.Enable();
    }

    private void OnDisable() {
        touchControls.Disable();
    }

    public bool GetRestartGame() {
        return Input.GetKeyDown(KeyCode.R);
    }

    public bool GetPauseGame() {
        return Input.GetKeyDown(KeyCode.P);
    }

    public bool GetQuitGame() {
        return Input.GetKeyDown(KeyCode.Escape);
    }

    public void SetInGame()
    {
        isInGame = true;
    }

    public void SetNotInGame()
    {
        isInGame = false;
    }

    public bool IsTouched() {
        return touchControls.Touch.TouchPress.IsPressed();
    }

    public Vector3 GetTouchPosition() {
        return touchControls.Touch.TouchPosition.ReadValue<Vector2>();
    }
}
