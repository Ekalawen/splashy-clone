using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.VFX;
//using UnityEngine.Rendering.PostProcessing;

public class PostProcessManager : MonoBehaviour {

    public GameObject bounceParticlePrefab;
    public GameObject catchBoostParticlePrefab;
    public GameObject catchColorChangerParticlePrefab;

    protected GameManager gm;
    protected Transform particlesFolder;

    public void Initialize() {
        gm = GameManager.Instance;
        particlesFolder = new GameObject("Particles").transform;
        particlesFolder.SetParent(transform);
        gm.map.onReachPlatforme.AddListener(StartBounceParticle);
        gm.map.onTriggerBoost.AddListener(StartCatchBoostParticlePrefab);
        gm.map.onTriggerColorChanger.AddListener(StartCatchColorChangerParticlePrefab);
    }

    public void StartBounceParticle(Plateforme plateforme, Vector3 position) {
        ParticleSystem ps = Instantiate(bounceParticlePrefab, position, Quaternion.identity, parent: particlesFolder).GetComponent<ParticleSystem>();
        ps.transform.LookAt(ps.transform.position + Vector3.right);
        Destroy(ps.gameObject, 1.0f);
    }

    public void StartCatchBoostParticlePrefab(Boost boost) {
        ParticleSystem ps = Instantiate(catchBoostParticlePrefab, boost.transform.position, Quaternion.identity, parent: particlesFolder).GetComponent<ParticleSystem>();
        ps.transform.LookAt(ps.transform.position + Vector3.right);
        Destroy(ps.gameObject, 1.0f);
    }

    public void StartCatchColorChangerParticlePrefab(ColorChanger colorChanger) {
        ParticleSystem ps = Instantiate(catchColorChangerParticlePrefab, colorChanger.transform.position, Quaternion.identity, parent: particlesFolder).GetComponent<ParticleSystem>();
        ps.transform.LookAt(ps.transform.position + Vector3.right);
        ParticleSystem.MainModule main = ps.main;
        main.startColor = new ParticleSystem.MinMaxGradient(colorChanger.GetColor());
        Destroy(ps.gameObject, 1.0f);
    }
}
