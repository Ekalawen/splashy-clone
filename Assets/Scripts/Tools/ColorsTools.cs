using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Assertions;

public static class ColorsTools
{
    public static Color SetHue(Color color, float newHue, bool hdr = true)
    {
        // newHue is in [0, 1] !
        float H, S, V;
        Color.RGBToHSV(color, out H, out S, out V);
        Color newColor = Color.HSVToRGB(newHue, S, V, hdr: hdr);
        return newColor;
    }

    public static float GetHue(Color color) {
        // newHue is in [0, 1] !
        float H, S, V;
        Color.RGBToHSV(color, out H, out S, out V);
        return H;
    }

    public static Color SetSaturation(Color color, float newSaturation, bool hdr = true) {
        // newSaturation is in [0, 1] !
        float H, S, V;
        Color.RGBToHSV(color, out H, out S, out V);
        Color newColor = Color.HSVToRGB(H, newSaturation, V, hdr: hdr);
        return newColor;
    }

    public static Color SetAlpha(Color color, float newAlpha)
    {
        return new Color(color.r, color.g, color.b, newAlpha);
    }

    public static float RandomHue()
    {
        return UnityEngine.Random.Range(0.0f, 1.0f);
    }
}