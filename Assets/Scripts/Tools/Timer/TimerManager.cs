using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class TimerManager : MonoBehaviour
{

    public static float FIXED_DELTA_TIME = 1f / 120f;
    public static int FRAME_RATE = 60;

    protected GameManager gm;
    protected Timer realGameTimer; // On ne peut pas réinitialiser celui-ci !

    public void Initialize() {
        gm = GameManager.Instance;
        name = "TimerManager";
        SetFrameRate();
        realGameTimer = new Timer();
        gm.scoreManager.onScoreChange.AddListener(UpdateSpeedBasedOnScore);
    }

    protected void UpdateSpeedBasedOnScore() {
        int score = gm.scoreManager.CurrentScore();
        int increment = gm.scoreManager.GetIncrementScore();
        int nonReseting = gm.scoreManager.GetNonResetingScoreIncrement();
        float logInside = score / Mathf.Pow(nonReseting, 2) * increment;
        float timeMultiplier = Mathf.Max(1, Mathf.Log(logInside) - 2.61f);
        //Debug.Log($"TimeMultiplier = {timeMultiplier}");
        Time.timeScale = timeMultiplier;
        Time.fixedDeltaTime = FIXED_DELTA_TIME * Time.timeScale;
    }

    public void SetFrameRate()
    {
        Application.targetFrameRate = FRAME_RATE;
        Time.fixedDeltaTime = FIXED_DELTA_TIME; // FixedUpdates are really small and are permorfed really fast, so we can do a lot of them ! (120 instead of 50 :))
    }

    public static string TimerToString(float seconds)
    {
        int secondes = Mathf.FloorToInt(seconds);
        int centiseconds = Mathf.FloorToInt((seconds - secondes) * 100);
        return secondes + ":" + centiseconds.ToString("D2");
    }
    public static string TimerToClearString(float time)
    {
        int seconds = Mathf.FloorToInt(time);
        int deciseconds = Mathf.FloorToInt((time - seconds) * 10);
        int centiseconds = Mathf.FloorToInt((time - seconds) * 100);
        if (time >= 3.0f)
            return seconds.ToString();
        if (time >= 1.0f)
            return seconds + "." + deciseconds.ToString("D1");
        else
            return seconds + "." + centiseconds.ToString("D2");
    }
    public static string TimerToClearerString(float time)
    {
        int seconds = Mathf.FloorToInt(time);
        int deciseconds = Mathf.FloorToInt((time - seconds) * 10);
        int centiseconds = Mathf.FloorToInt((time - seconds) * 100);
        if (time >= 2.0f)
            return seconds.ToString();
        else
            return seconds + "." + deciseconds.ToString("D1");
    }

    public float GetRealElapsedTime()
    {
        return realGameTimer.GetElapsedTime();
    }

    public Timer GetRealGameTimer()
    {
        return realGameTimer;
    }
}
