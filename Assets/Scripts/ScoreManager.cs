using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour {

    protected GameManager gm;
    protected int score;
    protected int scoreIncrement;
    protected int nonResetingScoreIncrement;
    [HideInInspector]
    public UnityEvent onScoreChange = new UnityEvent();

    public void Initialize() {
        gm = GameManager.Instance;
        score = 0;
        nonResetingScoreIncrement = 1;
        ResetScoreIncrement();
    }

    public void ResetScoreIncrement() {
        scoreIncrement = 1;
    }

    public void IncrementScoreIncrement() {
        scoreIncrement++;
        nonResetingScoreIncrement++;
    }

    public void IncrementScore() {
        score += scoreIncrement;
        onScoreChange.Invoke();
    }

    public int CurrentScore() {
        return score;
    }

    public int GetIncrementScore() {
        return scoreIncrement;
    }

    public int GetNonResetingScoreIncrement() {
        return nonResetingScoreIncrement;
    }
}
