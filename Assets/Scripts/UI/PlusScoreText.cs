using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlusScoreText : MonoBehaviour {

    public TMP_Text text;
    public float verticalOffset = 1.0f;
    public List<string> niceWords;

    [Header("Disapearing")]
    public float disapearDuration;
    public AnimationCurve disapearCurve;

    public void Initialize(Vector3 position, int value, bool useAdjectif) {
        text.text = GetTextFor(value, useAdjectif);
        text.transform.position = position;
        StartCoroutine(CDisapear());
    }

    protected IEnumerator CDisapear() {
        Timer timer = new Timer(disapearDuration);
        Color startColor = text.color;
        while(!timer.IsOver()) {
            float alpha = disapearCurve.Evaluate(timer.GetAvancement());
            text.color = ColorsTools.SetAlpha(startColor, alpha);
            yield return null;
        }
        Destroy(gameObject);
    }

    protected string GetTextFor(int value, bool useAdjectif) {
        string t = $"+{value}";
        if(useAdjectif) {
            t = $"{GetNiceWord()}\n{t}";
        }
        return t;
    }

    protected string GetNiceWord() {
        return MathTools.ChoseOne(niceWords);
    }
}
