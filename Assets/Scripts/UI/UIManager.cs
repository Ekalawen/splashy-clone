using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class UIManager : MonoBehaviour {

    public TMP_Text scoreText;
    public GameObject plusScoreTextPrefab;

    protected GameManager gm;
    protected Transform worldTextFolder;

    public void Initialize() {
        gm = GameManager.Instance;
        worldTextFolder = new GameObject("WordlTexts").transform;
        worldTextFolder.SetParent(transform);
        gm.scoreManager.onScoreChange.AddListener(UpdateScoreText);
        UpdateScoreText();
        gm.map.onReachPlatforme.AddListener(AddPlusScoreTextForReachPlateforme);
        gm.map.onTriggerBoost.AddListener(AddPlusScoreTextForReachCatchBoost);
    }

    protected void AddPlusScoreTextForReachPlateforme(Plateforme plateforme, Vector3 position) {
        if(plateforme.GetHadBoostCaptured()) {
            return; // Let the CatchBoost trigger display the text :)
        }
        PlusScoreText plus = Instantiate(plusScoreTextPrefab).GetComponent<PlusScoreText>();
        plus.transform.SetParent(worldTextFolder);
        plus.Initialize(plateforme.AbovePos(plus.verticalOffset), gm.scoreManager.GetIncrementScore(), useAdjectif: false);
    }

    protected void AddPlusScoreTextForReachCatchBoost(Boost boost) {
        PlusScoreText plus = Instantiate(plusScoreTextPrefab).GetComponent<PlusScoreText>();
        plus.transform.SetParent(worldTextFolder);
        Plateforme plateforme = boost.GetComponentInParent<Plateforme>();
        plus.Initialize(plateforme.AbovePos(plus.verticalOffset), gm.scoreManager.GetIncrementScore(), useAdjectif: true);
    }

    public void UpdateScoreText() {
        int score = gm.scoreManager.CurrentScore();
        scoreText.text = score.ToString();
    }
}
