using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_Automatic : PlayerController
{
    public override void StartMoving() {
        StartCoroutine(CMoving());
    }

    protected IEnumerator CMoving() {
        while(true) {
            yield return StartCoroutine(CMoveToNextPlateforme());
        }
    }

    protected virtual IEnumerator CMoveToNextPlateforme() {
        Vector3 abovePlateformePos = gm.map.GetNextPlateforme().AbovePos();
        Vector3 startPosition = player.transform.position;
        Vector3 maxJumpPos = GetMaxJumpPos(abovePlateformePos, startPosition);
        Timer timer = new Timer(jumpDuration);
        while(!timer.IsOver()) {
            float avancement = timer.GetAvancement();
            float x = MathCurves.Linear(startPosition.x, abovePlateformePos.x, avancement);
            float y = MathCurves.BezierCurve(startPosition.y, maxJumpPos.y, abovePlateformePos.y, avancement);
            float z = MathCurves.Linear(startPosition.z, abovePlateformePos.z, avancement);
            player.transform.position = new Vector3(x, y, z);
            yield return null;
        }
        player.transform.position = abovePlateformePos;
        gm.map.CrossNextPlateforme();
    }

    protected Vector3 GetMaxJumpPos(Vector3 abovePlateformePos, Vector3 startPosition) {
        Vector3 pos = (abovePlateformePos + startPosition) / 2;
        pos.y = abovePlateformePos.y + jumpOffset;
        return pos;
    }
}
