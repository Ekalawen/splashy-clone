using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController_Keyboard : PlayerController_Automatic {

    public float speed = 1.0f;

    protected bool hasCrossedNextPlateforme = false;

    public override void Initialize(Player player) {
        base.Initialize(player);
    }

    public override void StartMoving() {
        StartCoroutine(CStartMovingOnFirstTouch());
    }

    public IEnumerator CStartMovingOnFirstTouch() {
        while(true) {
            if(GetAxis() != 0) {
                break;
            }
            yield return null;
        }
        StartCoroutine(CMoving());
    }

    protected override IEnumerator CMoveToNextPlateforme() {
        Plateforme nextPlateforme = gm.map.GetNextPlateforme();
        hasCrossedNextPlateforme = false;
        nextPlateforme.onReached.AddListener(SetHasCrossedPlateforme);
        Vector3 abovePlateformePos = nextPlateforme.AbovePos();
        Vector3 startPosition = player.transform.position;
        Vector3 maxJumpPos = GetMaxJumpPos(abovePlateformePos, startPosition);
        Timer timer = new Timer(jumpDuration);
        while(true) {
            float avancement = timer.GetAvancement();
            float x = MathCurves.Linear(startPosition.x, abovePlateformePos.x, avancement);
            float y = MathCurves.BezierCurve(startPosition.y, maxJumpPos.y, abovePlateformePos.y, avancement);
            float z = GetZMovement();
            player.transform.position = new Vector3(x, y, z);
            yield return null;
            if(hasCrossedNextPlateforme) {
                break;
            }
        }
        gm.map.CrossNextPlateforme();
    }

    protected void SetHasCrossedPlateforme(Vector3 pos) {
        hasCrossedNextPlateforme = true;
    }

    protected float GetZMovement() {
        float current = player.pos.z;
        return current + GetAxis() * speed * Time.deltaTime;
    }

    public float GetAxis() {
        return -Input.GetAxis("Horizontal");
    }
}
