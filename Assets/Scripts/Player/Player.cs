using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public PlayerController controller;
    public GameObject sphere;
    public float deathOffset = 5.0f;

    protected GameManager gm;

    public void Initialize() {
        gm = GameManager.Instance;
        controller.Initialize(this);
        SetInitialPosition();
    }

    public Vector3 pos {
        get { return transform.position; }
        set { transform.position = value; }
    }


    protected void SetInitialPosition() {
        pos = gm.map.GetFirstPlateforme().AbovePos();
    }

    public float Radius() {
        return sphere.transform.localScale.y / 2;
    }

    public void Update() {
        CheckForDeath();
    }

    protected void CheckForDeath() {
        float minY = gm.map.GetMinPlateformeHeight();
        if (pos.y + deathOffset < minY) {
            gm.RestartGame();
        }
    }

    public void OnCollisionEnter(Collision collision) {
        if(collision.gameObject.GetComponent<ColorChanger>() != null) {
            return;
        }
        if(collision.gameObject.GetComponent<Boost>() != null) {
            return;
        }
        Plateforme plateforme = collision.gameObject.GetComponentInParent<Plateforme>();
        if(plateforme == null) {
            return;
        }
        plateforme.Reached(pos);
    }
}
