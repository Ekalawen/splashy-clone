using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerController_Touch : PlayerController_Automatic {

    public float speed = 1.0f;
    public float expandViewportCoef = 1.0f;

    protected bool hasCrossedNextPlateforme = false;

    public override void Initialize(Player player) {
        base.Initialize(player);
    }

    public override void StartMoving() {
        StartCoroutine(CStartMovingOnFirstTouch());
    }

    public IEnumerator CStartMovingOnFirstTouch() {
        while (true) {
            if (HasTouched()) {
                break;
            }
            yield return null;
        }
        StartCoroutine(CMoving());
    }

    protected bool HasTouched() {
        if(Input.touchSupported) {
            return gm.inputManager.IsTouched();
        }
        return Input.anyKeyDown;
    }

    protected override IEnumerator CMoveToNextPlateforme() {
        Plateforme nextPlateforme = gm.map.GetNextPlateforme();
        hasCrossedNextPlateforme = false;
        nextPlateforme.onReached.AddListener(SetHasCrossedPlateforme);
        Vector3 abovePlateformePos = nextPlateforme.AbovePos();
        Vector3 startPosition = player.transform.position;
        Vector3 maxJumpPos = GetMaxJumpPos(abovePlateformePos, startPosition);
        Timer timer = new Timer(jumpDuration);
        while(true) {
            float avancement = timer.GetAvancement();
            float x = MathCurves.Linear(startPosition.x, abovePlateformePos.x, avancement);
            float y = MathCurves.BezierCurve(startPosition.y, maxJumpPos.y, abovePlateformePos.y, avancement);
            float z = GetTouchZPosition();
            player.transform.position = new Vector3(x, y, z);
            yield return null;
            if(hasCrossedNextPlateforme) {
                break;
            }
        }
        gm.map.CrossNextPlateforme();
    }

    protected void SetHasCrossedPlateforme(Vector3 pos) {
        hasCrossedNextPlateforme = true;
    }

    public float GetTouchZPosition() {
        Vector3 screenPosition;
        if (Input.touchSupported) {
            if (!gm.inputManager.IsTouched()) {
                return player.pos.z;
            }
            screenPosition = gm.inputManager.GetTouchPosition();
        } else {
            screenPosition = Input.mousePosition;
        }
        Vector3 viewportPosition = Camera.main.ScreenToViewportPoint(screenPosition);
        viewportPosition = viewportPosition * 2 - Vector3.one;
        return - viewportPosition.x * gm.map.maxPlateformeLateralOffset * expandViewportCoef;
    }
}
