using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerController : MonoBehaviour {

    public float jumpDuration = 0.4f;
    public float jumpOffset = 0.6f;

    protected GameManager gm;
    protected Player player;

    public virtual void Initialize(Player player) {
        this.gm = GameManager.Instance;
        this.player = player;
        StartMoving();
    }

    public abstract void StartMoving();
}
