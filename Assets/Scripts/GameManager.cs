using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    /// Reference to this script
    /// See http://clearcutgames.net/home/?p=437 for singleton pattern.
    // Returns _instance if it exists, otherwise create one and set it has current _instance
    static GameManager _instance;
    public static GameManager Instance { get { return _instance ?? (_instance = new GameObject().AddComponent<GameManager>()); } }

    public static bool IsInGame { get { return _instance != null; } }

    [Header("Managers")]
    public GameObject playerPrefab; // Pour g�rer le joueur !
    public GameObject mapManagerPrefab; // Pour g�rer la map !
    public GameObject objectManagerPrefab; // Pour g�rer les objets !
    public GameObject cameraManagerPrefab; // Pour g�rer la cam�ra !
    public GameObject soundManagerPrefab; // Pour g�rer les sons et musiques !
    public GameObject postProcessManagerPrefab; // Pour g�rer les posteffects !
    public GameObject timerManagerPrefab; // Pour g�rer le timer !
    public GameObject scoreManagerPrefab; // Pour g�rer le score !
    public GameObject uiManagerPrefab; // Pour g�rer l'UI !

    [HideInInspector]
    public Player player;
    [HideInInspector]
    public MapManager map;
    [HideInInspector]
    public ObjectManager objectManager;
    [HideInInspector]
    public CameraManager cameraManager;
    [HideInInspector]
    public SoundManager soundManager;
    [HideInInspector]
    public PostProcessManager postProcessManager;
    [HideInInspector]
    public TimerManager timerManager;
    [HideInInspector]
    public ScoreManager scoreManager;
    [HideInInspector]
    public UIManager uiManager;
    [HideInInspector]
    public InputManager inputManager;
    [HideInInspector]
    public GameObject managerFolder;
    [HideInInspector]
    public bool partieDejaTerminee = false;
    protected bool isPaused = false;
    protected bool initializationIsOver = false;
    [HideInInspector]
    public UnityEvent onInitilizationFinish;
    [HideInInspector]
    public UnityEvent onFirstFrame;

    void Awake()
    {
        if (!_instance)
        {
            _instance = this;
        }
    }

    void Start()
    {
        managerFolder = new GameObject("Managers");
        timerManager = Instantiate(timerManagerPrefab, managerFolder.transform).GetComponent<TimerManager>();
        map = Instantiate(mapManagerPrefab, managerFolder.transform).GetComponent<MapManager>();
        player = Instantiate(playerPrefab).GetComponent<Player>();
        //objectManager = Instantiate(objectManagerPrefab, managerFolder.transform).GetComponent<ObjectManager>();
        cameraManager = Instantiate(cameraManagerPrefab, managerFolder.transform).GetComponent<CameraManager>();
        soundManager = Instantiate(soundManagerPrefab, managerFolder.transform).GetComponent<SoundManager>();
        postProcessManager = Instantiate(postProcessManagerPrefab, managerFolder.transform).GetComponent<PostProcessManager>();
        scoreManager = Instantiate(scoreManagerPrefab, managerFolder.transform).GetComponent<ScoreManager>();
        uiManager = Instantiate(uiManagerPrefab, managerFolder.transform).GetComponent<UIManager>();
        inputManager = InputManager.Instance;

        Initialize();
    }


    protected virtual void Initialize()
    {
        timerManager.Initialize();
        map.Initialize();
        player.Initialize();
        //objectManager.Initialize();
        cameraManager.Initialize();
        soundManager.Initialize();
        postProcessManager.Initialize();
        scoreManager.Initialize();
        uiManager.Initialize();
        inputManager.SetInGame();
        //FinishInitialization();
        //CallEventsOneFrameAfterFinishInitialization();
    }

    private void FinishInitialization() {
        initializationIsOver = true;
        onInitilizationFinish.Invoke();
    }

    protected void CallEventsOneFrameAfterFinishInitialization() {
        StartCoroutine(CCallEventsOneFrameAfterFinishInitialization());
    }

    protected IEnumerator CCallEventsOneFrameAfterFinishInitialization() {
        yield return null;
        onFirstFrame.Invoke();
    }

    void Update() {
        CheckRestartGame();

        CheckPauseToggling();
    }

    protected void CheckPauseToggling() {
        if (inputManager.GetPauseGame()) {
            if (!isPaused) {
                //uiManager.OpenMenu();
                Pause();
            } else {
                //uiManager.CloseMenu();
                UnPause();
            }
        }
    }

    public void Pause() {
        isPaused = true;
        Time.timeScale = 0.0f;
        soundManager.PauseSounds();
    }

    public void UnPause() {
        isPaused = false;
        Time.timeScale = 1.0f;
        soundManager.UnPauseSounds();
    }

    public bool IsPaused() {
        return isPaused;
    }

    protected void CheckRestartGame() {
        if (inputManager.GetRestartGame()) {
            RestartGame();
        }
        if (inputManager.GetQuitGame()) {
            QuitGame();
        }
    }

    public void RestartGame() {
        Time.timeScale = 1.0f;
        string sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public bool IsInitializationOver()
    {
        return initializationIsOver;
    }
}


