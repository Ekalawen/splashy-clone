using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundBankIngame", menuName = "SoundBankIngame")]
public class SoundBankInGame : ScriptableObject
{
    public AudioClipParams bouingClips;

    public AudioClipParams catchBoostBefore5;
    public AudioClipParams catchBoostAfter5;
    public AudioClipParams missBoost;

    public AudioClipParams catchColorChanger;
}
