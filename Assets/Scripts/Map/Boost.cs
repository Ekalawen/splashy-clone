using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Boost : MonoBehaviour {

    public GameObject cube;
    public float offsetAbovePlateforme = 0.5f;

    protected GameManager gm;
    protected Plateforme plateforme;

    [HideInInspector]
    public UnityEvent<Boost> onTrigger = new UnityEvent<Boost>();

    public void Initialize(Plateforme plateforme) {
        this.gm = GameManager.Instance;
        this.plateforme = plateforme;
        transform.position = plateforme.AbovePos(Radius() + offsetAbovePlateforme);
        transform.SetParent(plateforme.transform);
        plateforme.onReached.AddListener(ResetScoreIncrement);
    }

    public void Update() {
        transform.position = plateforme.AbovePos(Radius() + offsetAbovePlateforme);
    }

    private void OnCollisionEnter(Collision collision) {
        Player player = collision.gameObject.GetComponent<Player>();
        if(player == null) {
            return;
        }
        Trigger();
    }

    protected void Trigger() {
        gm.scoreManager.IncrementScoreIncrement();
        gm.soundManager.PlayCatchBoost();
        plateforme.SetHadBoostCaptured();
        onTrigger.Invoke(this);
        gm.map.onTriggerBoost.Invoke(this);
        Destroy(gameObject);
    }

    public float Radius() {
        return cube.transform.localPosition.y / 2.0f;
    }

    protected void ResetScoreIncrement(Vector3 plateformePos) {
        gm.scoreManager.ResetScoreIncrement();
        gm.soundManager.PlayResetScoreIncrement();
        Destroy(cube.GetComponent<Collider>());
    }
}
