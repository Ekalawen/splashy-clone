using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColorChanger : MonoBehaviour {
    public GameObject sphere;
    public float offsetAbovePlateforme = 0.5f;
    public float colorSpeed = 0.01f;
    public float durationBetweenPlateformesWhenChangingColor = 0.1f;

    protected GameManager gm;
    protected Plateforme plateforme;
    protected Renderer renderer;

    [HideInInspector]
    public UnityEvent<ColorChanger> onTrigger = new UnityEvent<ColorChanger>();

    public void Initialize(Plateforme plateforme) {
        this.gm = GameManager.Instance;
        this.plateforme = plateforme;
        transform.position = plateforme.AbovePos(Radius() + offsetAbovePlateforme);
        transform.SetParent(plateforme.transform);
        this.renderer = sphere.GetComponent<Renderer>();
        InitMaterial();
    }

    protected void InitMaterial() {
        renderer.material = new Material(renderer.material);
        renderer.material.color = ColorsTools.SetHue(renderer.material.color, ColorsTools.RandomHue(), hdr: true);
    }

    public void Update() {
        transform.position = plateforme.AbovePos(Radius() + offsetAbovePlateforme);
        RotateColor();
    }

    protected void RotateColor() {
        Color currentColor = renderer.material.color;
        float hue = ColorsTools.GetHue(currentColor);
        hue += (Time.deltaTime * colorSpeed) % 1.0f;
        renderer.material.color = ColorsTools.SetHue(currentColor, hue, hdr: true);
    }

    private void OnCollisionEnter(Collision collision) {
        Player player = collision.gameObject.GetComponent<Player>();
        if (player == null) {
            return;
        }
        Trigger();
    }

    protected void Trigger() {
        onTrigger.Invoke(this);
        gm.soundManager.PlayCatchColorChanger();
        gm.map.onTriggerColorChanger.Invoke(this);
        gm.map.ChangeHueOfFollowingPlateformes(GetComponentInParent<Plateforme>(), GetHue(), durationBetweenPlateformesWhenChangingColor);
        Destroy(gameObject);
    }

    public float Radius() {
        return sphere.transform.localPosition.y / 2.0f;
    }

    public Color GetColor() {
        return renderer.material.color;
    }

    public float GetHue() {
        return ColorsTools.GetHue(GetColor());
    }
}
