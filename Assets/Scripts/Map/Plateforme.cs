using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Plateforme : MonoBehaviour {

    public GameObject top;
    public GameObject pillar;

    [Header("Start Animation")]
    public float startOffset = -100;
    public float startAnimationDuration = 0.4f;
    public AnimationCurve startAnimationCurve;

    [Header("Down Animation")]
    public float downOffset = -10;
    public float downAnimationDuration = 0.4f;
    public AnimationCurve downAnimationCurve;

    [Header("Color")]
    public float colorSaturation = 0.75f;

    protected GameManager gm;
    protected bool hasBeenReached = false;
    protected Vector3 initialPos;
    [HideInInspector]
    public UnityEvent<Vector3> onReached = new UnityEvent<Vector3>();
    protected bool hadBoostCaptured = false;

    public void Initialize() {
        gm = GameManager.Instance;
        this.initialPos = pos;
        StartCoroutine(CGoUpOnStart());
    }

    protected IEnumerator CGoUpOnStart() {
        Vector3 startPos = pos + Vector3.up * startOffset;
        Timer timer = new Timer(startAnimationDuration);
        while(!timer.IsOver()) {
            pos = Vector3.LerpUnclamped(startPos, initialPos, startAnimationCurve.Evaluate(timer.GetAvancement()));
            yield return null;
        }
        pos = initialPos;
    }

    public Vector3 pos {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Vector3 AbovePos(float radius = -1) {
        if(radius == -1) {
            radius = gm.player.Radius();
        }
        return initialPos + Vector3.up * (radius + top.transform.localScale.y / 2);
    }

    public void Reached(Vector3 playerPos) {
        if(!hasBeenReached) {
            hasBeenReached = true;
            Vector3 abovePos = AbovePos(radius: 0);
            Vector3 contactPoint = Vector3.ProjectOnPlane(playerPos - abovePos, Vector3.up) + abovePos;
            onReached.Invoke(contactPoint);
            gm.map.onReachPlatforme.Invoke(this, contactPoint);
            gm.soundManager.PlayReachPlateformeSound();
            StartCoroutine(CGoDown());
        }
    }

    protected IEnumerator CGoDown() {
        Vector3 startPos = pos;
        Vector3 targetPos = startPos + Vector3.up * downOffset;
        Timer timer = new Timer(downAnimationDuration);
        while (!timer.IsOver()) {
            pos = Vector3.LerpUnclamped(startPos, targetPos, downAnimationCurve.Evaluate(timer.GetAvancement()));
            yield return null;
        }
        pos = targetPos;
    }

    public void SetHue(float hue) {
        Material topMaterial = top.GetComponent<Renderer>().material;
        topMaterial.color = ColorsTools.SetSaturation(topMaterial.color, colorSaturation, hdr: true);
        topMaterial.color = ColorsTools.SetHue(topMaterial.color, hue, hdr: true);
        Material pillarMaterial = pillar.GetComponent<Renderer>().material;
        pillarMaterial.color = ColorsTools.SetSaturation(pillarMaterial.color, colorSaturation, hdr: true);
        pillarMaterial.color = ColorsTools.SetHue(pillarMaterial.color, hue, hdr: true);
    }

    public void SetHadBoostCaptured() {
        hadBoostCaptured = true;
    }

    public bool GetHadBoostCaptured() {
        return hadBoostCaptured;
    }

    public void SetCantBeReached() {
        hasBeenReached = true;
    }
}
