using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class MapManager : MonoBehaviour {

    [Header("Plateforme spawning")]
    public GameObject plateformePrefab;
    public int nbForwardPlateformes = 10;
    public int nbBackwardPlateformes = 3;
    public float forwardDistanceBetweenPlateformes = 2.0f;
    public float plateformeVerticalCoef = 0.1f;
    public float maxPlateformeLateralOffset = 1.5f;
    public float maxPlateformeVerticalOffset = 0.5f;

    [Header("Power ups")]
    public GameObject boostPrefab;
    public float boostProbability = 0.2f;
    public GameObject colorChangerPrefab;
    public float colorChangerProbabiliity = 0.2f;

    protected GameManager gm;
    protected Transform plateformeFolder;
    protected List<Plateforme> plateformes;
    protected int nbPlateformesSpawned = 0;
    protected int nbPlateformesCrossed = 0;
    protected int nbPlateformesDestroyed = 0;
    protected float baseHue = -1;

    [HideInInspector]
    public UnityEvent<Plateforme, Vector3> onReachPlatforme = new UnityEvent<Plateforme, Vector3>();
    [HideInInspector]
    public UnityEvent<Boost> onTriggerBoost = new UnityEvent<Boost>();
    [HideInInspector]
    public UnityEvent<ColorChanger> onTriggerColorChanger = new UnityEvent<ColorChanger>();

    public void Initialize() {
        gm = GameManager.Instance;
        SpawnInitialMap();
    }

    protected void SpawnInitialMap() {
        plateformeFolder = new GameObject("Plateformes").transform;
        plateformes = new List<Plateforme>();
        for(int i = 0; i < nbForwardPlateformes; ++i) {
            SpawnNextPlateforme();
        }
    }

    protected void SpawnNextPlateforme() {
        Vector3 pos = GetNextPlateformePosition();
        Plateforme plateforme = Instantiate(plateformePrefab, pos, Quaternion.identity, parent: plateformeFolder).GetComponent<Plateforme>();
        plateforme.Initialize();
        if(baseHue != -1) {
            plateforme.SetHue(baseHue);
        }
        plateformes.Add(plateforme);
        nbPlateformesSpawned++;
        if(nbPlateformesSpawned == 1) {
            plateforme.SetCantBeReached();
        }
        TrySpawnBoostOn(plateforme);
        TrySpawnColorChangerOn(plateforme);
    }

    protected void TrySpawnBoostOn(Plateforme plateforme) {
        if(UnityEngine.Random.Range(0.0f, 1.0f) <= boostProbability) {
            SpawnBoostOn(plateforme);
        }
    }

    protected void TrySpawnColorChangerOn(Plateforme plateforme) {
        if(plateforme.GetComponentInChildren<Boost>() != null) {
            return;
        }
        if(UnityEngine.Random.Range(0.0f, 1.0f) <= colorChangerProbabiliity) {
            SpawnColorChangerOn(plateforme);
        }
    }

    public void SetBaseHue(float hue) {
        baseHue = hue;
    }

    public Plateforme NextPlateforme(Plateforme plateforme) {
        int index = plateformes.IndexOf(plateforme);
        if (index == -1 || index == plateformes.Count - 1)
            return null;
        return plateformes[index + 1];
    }

    protected void SpawnBoostOn(Plateforme plateforme) {
        Boost boost = Instantiate(boostPrefab).GetComponent<Boost>();
        boost.Initialize(plateforme);
    }

    protected void SpawnColorChangerOn(Plateforme plateforme) {
        ColorChanger colorChanger = Instantiate(colorChangerPrefab).GetComponent<ColorChanger>();
        colorChanger.Initialize(plateforme);
    }

    protected Vector3 GetNextPlateformePosition() {
        if(plateformes.Count == 0) {
            return Vector3.zero;
        }
        Vector3 lastPos = plateformes.Last().pos;
        lastPos.x += forwardDistanceBetweenPlateformes;
        float lateralOffset = UnityEngine.Random.Range(-maxPlateformeLateralOffset, maxPlateformeLateralOffset);
        lastPos.z = lateralOffset;
        float verticalOffset = UnityEngine.Random.Range(-maxPlateformeVerticalOffset, maxPlateformeVerticalOffset);
        lastPos.y = nbPlateformesSpawned * plateformeVerticalCoef + verticalOffset;
        return lastPos;
    }

    public Plateforme GetNextPlateforme() {
        int index = nbPlateformesCrossed - nbPlateformesDestroyed + 1;
        return plateformes[index];
    }

    public Plateforme GetFirstPlateforme() {
        return plateformes.First();
    }

    public void CrossNextPlateforme() {
        SpawnNextPlateforme();
        if(nbPlateformesCrossed >= nbBackwardPlateformes) {
            DestroyLastPlateforme();
        }
        gm.scoreManager.IncrementScore();
        nbPlateformesCrossed++;
    }

    protected void DestroyLastPlateforme() {
        Plateforme lastPlateforme = plateformes.First();
        plateformes.Remove(lastPlateforme);
        Destroy(lastPlateforme.gameObject);
        nbPlateformesDestroyed++;
    }

    public void ChangeHueOfFollowingPlateformes(Plateforme startPlateforme, float hue, float interval) {
        StartCoroutine(CChangeHueOfFollowingPlateformes(startPlateforme, hue, interval));
    }

    protected IEnumerator CChangeHueOfFollowingPlateformes(Plateforme startPlateforme, float hue, float interval) {
        Plateforme currentPlateforme = startPlateforme;
        while(true) {
            currentPlateforme.SetHue(hue);
            yield return new WaitForSeconds(interval);
            currentPlateforme = NextPlateforme(currentPlateforme);
            if(currentPlateforme == null) {
                break;
            }
        }
        SetBaseHue(hue);
    }

    public float GetMinPlateformeHeight() {
        return plateformes.First().pos.y;
    }
}
