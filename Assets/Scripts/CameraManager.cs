using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public GameObject cameraPrefab;
    public float distanceToSpeedCoef = 0.3f;
    public Vector3 playerOffset = new Vector3(-3, 3, 0);
    public Vector3 cameraRotation = new Vector3(23, 90, 0);
    public Vector3 startOffset = new Vector3(-5, 5, 0);
    public float minXDistance = 5.0f;
    public float minXDistanceStrict = 2.5f;

    protected GameManager gm;
    protected Player player;
    protected new Camera camera;

    public void Initialize() {
        gm = GameManager.Instance;
        player = gm.player;
        camera = Instantiate(cameraPrefab, TargetPos() + startOffset, Quaternion.identity, parent: transform).GetComponent<Camera>();
        LookAtPlayer();
    }

    public void Update()
    {
        Vector3 targetPos = TargetPos();
        Vector3 currentPos = camera.transform.position;
        float magnitude = (targetPos - currentPos).magnitude;
        float speed = magnitude * distanceToSpeedCoef;
        Vector3 direction = (targetPos - currentPos).normalized;
        Vector3 movement = direction * speed * Time.deltaTime;
        if (movement.magnitude > magnitude)
        {
            movement = direction * magnitude;
        }
        movement.z = 0;
        movement = DecreaseMovementIfTooCloseToPlayer(movement);
        camera.transform.position += movement;
    }

    protected Vector3 DecreaseMovementIfTooCloseToPlayer(Vector3 movement) {
        float distance = player.pos.x - camera.transform.position.x;
        if(distance <= minXDistanceStrict) {
            return Vector3.zero;
        }
        if (distance <= minXDistance) {
            float avancement = MathCurves.LinearReversed(minXDistanceStrict, minXDistance, distance);
            avancement = Mathf.Pow(avancement, 3);
            return movement * avancement;
        } 
        return movement;
    }

    private void LookAtPlayer() {
        camera.transform.rotation = Quaternion.Euler(cameraRotation);
    }

    public Vector3 TargetPos() {
        return player.pos + playerOffset;
    }
}
